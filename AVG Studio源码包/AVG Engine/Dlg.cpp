//dlg.cpp

#include "stdafx.h"


//
CDlg::CDlg()
{
};
//
CDlg::~CDlg()
{
	DeleteDC( hMemDC );
};
//
BOOL CDlg::InitDlg(HWND hwnd,CGameData* pD,CMidi* pM)
{
	pDB = pD;
	pMidi = pM;
	HDC hdc = ::GetDC( hwnd );
	hMemDC = ::CreateCompatibleDC( hdc );
	HBITMAP hBmp = (HBITMAP)LoadImage( NULL, "dlg.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE );
	DeleteObject( ::SelectObject(hMemDC,hBmp) );
	::ReleaseDC( hwnd, hdc);
	nState = 0;
	nMode = 1;
	memset( str1,0x20,64 );
	memset( str2,0x20,64 );
	memset( str3,0x20,64 );
	memset( str4,0x20,64 );
	return TRUE;
};
//
BOOL CDlg::Open(char *FileName,int n,int mode)
{
	nState = 1;
	nKeyDown = 0;
	nMode = mode;
	memset( str1,0x20,64 );
	memset( str2,0x20,64 );
	memset( str3,0x20,64 );
	memset( str4,0x20,64 );
	int index1 = 0;
	int index2 = 0;
	int index3 = 0;
	int index4 = 0;
	//
	HANDLE hFile;
	hFile = CreateFile(FileName,GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,0,NULL);
	if(hFile==INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}
	DWORD dwActualSize = 1;
	char c;
	int nCurLine = 1;
	//读出指定行的内容
	while( dwActualSize != 0 )
	{
		ReadFile(hFile,&c,sizeof(char),&dwActualSize,NULL);
		if( c == 0x0D )
		{	//windows下回车是由“0x0D、0x0A”构成的
			ReadFile(hFile,&c,sizeof(char),&dwActualSize,NULL);
			nCurLine ++;
		}
		else
		{
			if( nCurLine == n )
			{
				str1[index1] = c;
				if( index1<60 )
					index1 ++;
			}
			else if( nCurLine == (n+1) )
			{
				str2[index2] = c;
				if( index2<60 )
					index2 ++;
			}
			else if( nCurLine == (n+2) )
			{
				str3[index3] = c;
				if( index3<60 )
					index3 ++;
			}
			else if( nCurLine == (n+3) )
			{
				str4[index4] = c;
				if( index4<60 )
					index4 ++;
			}
		}
	}//while语句结束
	//关闭文件
	CloseHandle(hFile);
	return TRUE;
};
//
BOOL CDlg::OnLButtonDown(int tx,int ty)
{
	nKeyDown = 0;
	if( nState == 6 )
	{
		if( tx>dlgx+16 && ty>dlgy+16 && tx<dlgx+dlgcx-32 && ty<dlgy+dlgcy-32 && nMode<2 )
		{	//无选项的对话框
			pMidi->PlayWav( "keypress.wav" );
			nState = 0;
			return TRUE;
		}
		else if( tx>b1x && ty>b1y && tx<(b1x+bcx) && ty<(b1y+bcy) && nMode>1 )
			nKeyDown = 1;
		else if( tx>b2x && ty>b2y && tx<(b2x+bcx) && ty<(b2y+bcy) && nMode>1 )
			nKeyDown = 2;
		else if( tx>b3x && ty>b3y && tx<(b3x+bcx) && ty<(b3y+bcy) && nMode>2 )
			nKeyDown = 3;
		else if( tx>b4x && ty>b4y && tx<(b4x+bcx) && ty<(b4y+bcy) && nMode>3 )
			nKeyDown = 4;
	}
	if( nKeyDown != 0 )
	{
		pMidi->PlayWav( "keypress.wav" );
		nState = 7;				//按钮按下状态
		pDB->SET(nKeyDown,8);	//保存选项状态
		return TRUE;
	}
	return FALSE;
}
//
void CDlg::Paint(HWND hwnd,HDC hdc)
{
	if( nState > 0 )
	{
		BLENDFUNCTION bf;
		bf.AlphaFormat = 0;
		bf.BlendFlags = 0;
		bf.BlendOp = AC_SRC_OVER;
		bf.SourceConstantAlpha = 200;
		//1、用Alpha混合的方式，绘制衬底
//		AlphaBlend( hdc,(dlgx+16),(dlgy+16),(dlgcx-32),(dlgcy-32),hMemDC,192,0,1,1,bf );
		StretchBlt( hdc,(dlgx+16),(dlgy+16),(dlgcx-32),(dlgcy-32),hMemDC,192,0,1,1,SRCAND );
		//2、绘制角图
		TransBlt( hdc,dlgx,dlgy,32,32,hMemDC,0,0,colorkey );
		TransBlt( hdc,dlgx,(dlgy+dlgcy-32),32,32,hMemDC,0,32,colorkey );
		TransBlt( hdc,(dlgx+dlgcx-32),dlgy,32,32,hMemDC,32,0,colorkey );
		TransBlt( hdc,(dlgx+dlgcx-32),(dlgy+dlgcy-32),32,32,hMemDC,32,32,colorkey );
		//3、绘制文字
		HFONT pFont=CreateFont(16,0,0,0,0,FALSE,FALSE,0,
			GB2312_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH|FF_SWISS,"宋体");
		DeleteObject( ::SelectObject(hdc,pFont) );
		SetBkMode( hdc,TRANSPARENT );
		//首先绘制阴影
		SetTextColor( hdc,0x00000000 );
		TextOut( hdc,91,353,str1,60 );
		TextOut( hdc,91,372,str2,60 );
		TextOut( hdc,91,391,str3,60 );
		TextOut( hdc,91,410,str4,60 );
		//然后绘制黄色的文字
		SetTextColor( hdc,0x0000ffff );
		TextOut( hdc,90,352,str1,60 );
		TextOut( hdc,90,371,str2,60 );
		TextOut( hdc,90,390,str3,60 );
		TextOut( hdc,90,409,str4,60 );
		SetTextColor( hdc,0x00000000 );
		//4、根据按键状况以及按下状况绘制按钮
		if( nMode > 1 )
		{
			if( nKeyDown == 1 ){
				TransBlt( hdc,b1x,b1y,bcx,bcy,hMemDC,128,0,colorkey );	}
			else{
				TransBlt( hdc,b1x,b1y,bcx,bcy,hMemDC,64,0,colorkey );	}
			if( nKeyDown == 2 ){
				TransBlt( hdc,b2x,b2y,bcx,bcy,hMemDC,128,32,colorkey );	}
			else{
				TransBlt( hdc,b2x,b2y,bcx,bcy,hMemDC,64,32,colorkey );	}
		}
		if( nMode > 2 )
		{
			if( nKeyDown == 3 ){
				TransBlt( hdc,b3x,b3y,bcx,bcy,hMemDC,128,64,colorkey );	}
			else{
				TransBlt( hdc,b3x,b3y,bcx,bcy,hMemDC,64,64,colorkey );	}
		}
		if( nMode > 3 )
		{
			if( nKeyDown == 4 ){
				TransBlt( hdc,b4x,b4y,bcx,bcy,hMemDC,128,96,colorkey );	}
			else{
				TransBlt( hdc,b4x,b4y,bcx,bcy,hMemDC,64,96,colorkey );	}
		}
		//
		if( nState>0 && nState<6 )
			nState ++;			//屏蔽鼠标事件的延时
		if( nState>6 && nState<9 )
			nState ++;			//按钮按下去以后的延时
		if( nState == 9 )
			nState = 0;
	}//if语句结束
};//Paint()函数结束
//
BOOL CDlg::IsBusy()
{
	if( nState == 0 )
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}
};
//the end.