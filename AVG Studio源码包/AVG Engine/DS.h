//DS.h

#if !defined(__DS_H__)
#define __DS_H__

class CBmp;

#include "Scene.h"		//场基类
#include "Bmp.h"		//图片类
#include "Dlg.h"		//对话框类

//
class CDS : public CScene	//从“场”类继承
{
public:
	CDS();
	~CDS();
	BOOL Init(HWND hwnd,CMidi* pMidi,CGameData* pDB);
	void OnLButtonDown(int tx,int ty);
	void OnTimerPrc(HWND hwnd);
	//用于检验指令的执行状态，只有不忙的时候才能执行下一条指令
	//图片的显示/消失，等待用户的鼠标响应，都属于忙状态
//继承的命令有：
//	BOOL  Open(char* FileName);		//从配置文件打开
//	BOOL  Save(char* FileName);		//保存入指定文件
//	ACT*  GetNextAct();				//获得下一条动作指令。
//	BOOL  CluAll(CGameData* pDB);	//执行出口过程的全部计算指令。
//
private:
	CMidi*  m_pMidi;				//声音播放类的指针
	CGameData*	m_pDB;				//系统数据堆
    CBmp	m_BkBmp;				//背景图片
	CBmp	m_Bmp[10];				//人物图片数组
	CDlg	m_Dlg;					//对话框
	HDC		m_hBufDC;				//主缓冲层面
	BOOL OnCode(HWND hwnd,ACT* pAct);	//执行一条动作指令
};

#endif