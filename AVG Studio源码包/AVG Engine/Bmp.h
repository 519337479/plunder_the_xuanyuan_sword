//Bmp.h

#if !defined(__DB_H__)
#define __DB_H__

#include "wingdi.h"
#pragma comment(lib,"msimg32.lib")
//
static int ALPHA[13] =
		{0,42,84,126,170,212,255,212,170,126,84,0,0};	//帧—透明度对照表
//
class CBmp
{
public:
	CBmp();
	~CBmp();
	BOOL InitBmp(HWND hwnd);
	BOOL Open(char *FileName,int sx,int sy,int scx,int scy);
	void Paint(HWND hwnd,HDC hdc);
	BOOL IsBusy();
	BOOL Close();
//
private:
	HDC hMemDC;			//用于存放操作位图的设备
	HDC hSMaskDC;		//源蒙板位图（ColorKey是黑的，图像是白的）
	HDC hDMaskDC;		//目标蒙板位图（ColorKey是白的，图像是黑的）
	HDC hBufDC;			//位图缓冲。不能修改hMemDC
	int x,y,cx,cy;		//位图的显示位置和长宽
	int nState;			//0:未使用;1~5,出现过程;6,正常;7~11,消失过程
	BLENDFUNCTION bf;	//用于Alpha混合
};

#endif