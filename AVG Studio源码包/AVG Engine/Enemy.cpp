//enemy.cpp

#include "stdafx.h"


//
CEnemy::CEnemy()
{
};
//
CEnemy::~CEnemy()
{
};
//
BOOL CEnemy::Init(HDC hBmpDC,CGameData* pD,CMidi* pM)
{
	hMemDC	= hBmpDC;
	m_pDB	= pD;
	m_pMidi = pM;
	x		= 0;
	y		= 0;
	npx		= 0;
	npy		= 0;
	npcx	= 0;
	npcy	= 0;
	nState	= 0;
	nType	= 0;
	nStartDelay = 0;
	nDelay	= 0;
	return TRUE;
};
//
BOOL CEnemy::Set(int mode,int tx,int ty,int time)
{
	nType	= mode;
	x		= tx;
	y		= ty;
	nStartDelay = time;
	nState = 1;
	return TRUE;
};
//
void CEnemy::Paint(HWND hwnd,HDC hdc)
{
	//如果处于不活动状态，则不进行任何操作
	if( nState == 0 )
		return;
	//首先根据当前显示区域和类型显示图片
	if( nState != 1 )	//等待出现状态不显示
	{
		int sx = 0;
		int sy = 0;		//确定源位置
		//
		switch( nState )
		{
		case 2:		//出现
		case 6:		//消失
			{
				switch( nType )
				{
				case 1:
				case 2:
					sx = X1A;
					sy = Y1A;
					break;
				case 3:
					sx = X1A+CX1-npcx;
					sy = Y1A;
					break;
				case 4:
				case 5:
					sx = X2A;
					sy = Y2A;
					break;
				case 6:
					sx = X2A+CX2-npcx;
					sy = Y2A;
					break;
				case 7:
				case 8:
					sx = X3A;
					sy = Y3A;
					break;
				case 9:
					sx = X3A+CX3-npcx;
					sy = Y3A;
					break;
				}
				break;
			}
		case 3:
		case 5:		//两种出现后的等待状态一定是全图，不出现显示一半的情况
			{
				switch( nType )
				{
				case 1:
				case 2:
				case 3:
					sx = X1A;
					sy = Y1A;
					break;
				case 4:
				case 5:
				case 6:
					sx = X2A;
					sy = Y2A;
					break;
				case 7:
				case 8:
				case 9:
					sx = X3A;
					sy = Y3A;
					break;
				}
				break;
			}
		case 4:		//射击一定是全图，不出现显示一半的情况
			{
				switch( nType )
				{
				case 1:
				case 2:
				case 3:
					sx = X1B;
					sy = Y1B;
					break;
				case 4:
				case 5:
				case 6:
					sx = X2B;
					sy = Y2B;
					break;
				case 7:
				case 8:
				case 9:
					sx = X3B;
					sy = Y3B;
					break;
				}
				break;
			}
		case 7:		//死亡一定是全图，不出现显示一半的情况
			{
				if( nDelay >(DELAY3/2) )		//第一帧
				{
					switch( nType )
					{
					case 1:
					case 2:
					case 3:
						sx = X1C;
						sy = Y1C;
						break;
					case 4:
					case 5:
					case 6:
						sx = X2C;
						sy = Y2C;
						break;
					case 7:
					case 8:
					case 9:
						sx = X3C;
						sy = Y3C;
						break;
					}
				}
				else				//第二帧
				{
					switch( nType )
					{
					case 1:
					case 2:
					case 3:
						sx = X1D;
						sy = Y1D;
						break;
					case 4:
					case 5:
					case 6:
						sx = X2D;
						sy = Y2D;
						break;
					case 7:
					case 8:
					case 9:
						sx = X3D;
						sy = Y3D;
						break;
					}
				}
				break;
			}
		}
		//
		TransBlt( hdc,npx,npy,npcx,npcy,hMemDC,sx,sy,colorkey );
	}//if语句结束
	//
	//然后根据延时以及状态调整各个参数
	switch( nState )
	{
	case 1:		//等待出现
		if( nStartDelay > 0 )
		{
			nStartDelay --;
		}
		else if( nDelay > 0 )
		{
			nDelay --;
		}
		else
		{	//出现
			nState = 2;
			nDelay = 4;
		}
		break;
	case 2:		//出现过程
		if( nDelay > 0 )
		{
			nDelay --;
		}
		else
		{	//完全显出
			nState = 3;
			nDelay = DELAY1;
		}
		break;
	case 3:		//出现后到开枪的间隙
		if( nDelay > 0 )
		{
			nDelay --;
		}
		else
		{	//开枪
			Fire();
			nState = 4;
			nDelay = 1;
		}
		break;
	case 4:		//开枪
		if( nDelay > 0 )
		{
			nDelay --;
		}
		else
		{
			nState = 5;
			nDelay = DELAY2;
		}
		break;
	case 5:		//开枪后到消失的间隙
		if( nDelay > 0 )
		{
			nDelay --;
		}
		else
		{	//进入消失过程
			nState = 6;
			nDelay = 4;
		}
		break;
	case 6:		//消失过程
		if( nDelay > 0 )
		{
			nDelay --;
		}
		else
		{	//进入等待过程
			nState = 1;
			nDelay = INTERVAL;
		}
		break;
	case 7:		//死亡过程
		if( nDelay > 0 )
		{
			nDelay --;
		}
		else
		{	//死亡动画演示后，进入不响应状态
			nState = 0;
		}
		break;
	}//switch语句结束
	//
	//设定出现和消失过程中显示区域的大小
	if( nState == 2 )
	{	//出现过程
		switch( nType )
		{
		case 1:		//向上出现
			npcx = CX1;
			npcy = CY1*(4-nDelay)/4;
			npx = x;
			npy = y+CY1-npcy;
			break;
		case 4:		//向上出现
			npcx = CX2;
			npcy = CY2*(4-nDelay)/4;
			npx = x;
			npy = y+CY2-npcy;
			break;
		case 7:		//向上出现
			npcx = CX3;
			npcy = CY3*(4-nDelay)/4;
			npx = x;
			npy = y+CY3-npcy;
			break;
		case 2:		//向左出现
			npcx = CX1*(4-nDelay)/4;
			npcy = CY1;
			npx = x+CX1-npcx;
			npy = y;
			break;
		case 5:		//向左出现
			npcx = CX2*(4-nDelay)/4;
			npcy = CY2;
			npx = x+CX2-npcx;
			npy = y;
			break;
		case 8:		//向左出现
			npcx = CX3*(4-nDelay)/4;
			npcy = CY3;
			npx = x+CX3-npcx;
			npy = y;
			break;
		case 3:		//向右出现
			npcx = CX1*(4-nDelay)/4;
			npcy = CY1;
			npx = x;
			npy = y;
			break;
		case 6:		//向右出现
			npcx = CX2*(4-nDelay)/4;
			npcy = CY2;
			npx = x;
			npy = y;
			break;
		case 9:		//向右出现
			npcx = CX3*(4-nDelay)/4;
			npcy = CY3;
			npx = x;
			npy = y;
			break;
		}
	}
	else if( nState == 6 )
	{	//消失过程
		switch( nType )
		{
		case 1:		//向上消失
			npcx = CX1;
			npcy = CY1*nDelay/4;
			npx = x;
			npy = y+CY1-npcy;
			break;
		case 4:		//向上消失
			npcx = CX2;
			npcy = CY2*nDelay/4;
			npx = x;
			npy = y+CY2-npcy;
			break;
		case 7:		//向上消失
			npcx = CX3;
			npcy = CY3*nDelay/4;
			npx = x;
			npy = y+CY3-npcy;
			break;
		case 2:		//向左消失
			npcx = CX1*nDelay/4;
			npcy = CY1;
			npx = x+CX1-npcx;
			npy = y;
			break;
		case 5:		//向左消失
			npcx = CX2*nDelay/4;
			npcy = CY2;
			npx = x+CX2-npcx;
			npy = y;
			break;
		case 8:		//向左消失
			npcx = CX3*nDelay/4;
			npcy = CY3;
			npx = x+CX3-npcx;
			npy = y;
			break;
		case 3:		//向右消失
			npcx = CX1*nDelay/4;
			npcy = CY1;
			npx = x;
			npy = y;
			break;
		case 6:		//向右消失
			npcx = CX2*nDelay/4;
			npcy = CY2;
			npx = x;
			npy = y;
			break;
		case 9:		//向右消失
			npcx = CX3*nDelay/4;
			npcy = CY3;
			npx = x;
			npy = y;
			break;
		}
	}//if语句结束
};//Paint()函数结束
//
BOOL CEnemy::OnShot(int tx,int ty)
{
	if( nState>1 && nState<7 )
	{	//从出现过程到消失过程
		if( tx>npx && tx<(npx+npcx) && ty>npy && ty<(npy+npcy) )
		{
			m_pMidi->PlayWav( "dead.wav" );
			nState = 7;		//转入死亡状态
			nDelay = DELAY3;
			//给玩家加分
			int money = m_pDB->Get( 3 );	//获得金钱
			switch( nType )
			{
			case 1:
			case 2:
			case 3:
				money += TYPE1COST;
				break;
			case 4:
			case 5:
			case 6:
				money += TYPE2COST;
				break;
			case 7:
			case 8:
			case 9:
				money += TYPE3COST;
				break;
			}
			m_pDB->SET( money, 3 );
			//
			return TRUE;
		}
	}
	return FALSE;
};
//
BOOL CEnemy::Fire()
{
	m_pMidi->PlayWav( "enemyshoot.wav" );
	//给玩家减血
	int blood = m_pDB->Get( 2 );	//获得生命值
	blood -= ENEMYPOWER;
	if( blood < 0 )
		blood = 0;
	m_pDB->SET( blood, 2 );
	//
	return TRUE;
};
//
BOOL CEnemy::IsLiving()
{
	if( nState==0 && nStartDelay==0 )
		return FALSE;
	else
		return TRUE;
}
//the end.