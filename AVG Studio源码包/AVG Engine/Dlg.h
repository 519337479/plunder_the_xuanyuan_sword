//Dlg.h
//
#if !defined(__DLG_H__)
#define __DLG_H__

#define dlgx	50		//x
#define dlgy	330		//y
#define dlgcx	540		//宽度
#define dlgcy	120		//高度
#define bcx		64
#define bcy		32
#define b1x		302
#define b1y		314
#define b2x		366
#define b2y		314
#define b3x		430
#define b3y		314
#define b4x		494
#define b4y		314
//
static COLORREF	colorkey = 0x00ff00ff;	//设置透明色
//
class CDlg
{
public:
	CDlg();
	~CDlg();
	BOOL InitDlg(HWND hwnd,CGameData* pD,CMidi* pM);
	BOOL Open(char *FileName,int n,int mode);
	//FileName文本文件名；n行数；mode选项的个数
	BOOL OnLButtonDown(int tx,int ty);
	void Paint(HWND hwnd,HDC hdc);
	BOOL IsBusy();
//
private:
	CGameData* pDB;		//系统数据堆
	CMidi* pMidi;		//用于播放声音
	HDC hMemDC;			//用于存放角图和按钮的设备
	int nState;			//0:未使用;计数到6才允许响应鼠标的操作
	int nMode;			//0和1:无选项的对话框
	int nKeyDown;		//标明被按下去的键
	char str1[64];		//第一行显示的文字
	char str2[64];		//第二行显示的文字
	char str3[64];		//第三行显示的文字
	char str4[64];		//第四行显示的文字
};

#endif