//Midi.h		用于播放背景音乐和WAV文件的类
//
#if !defined(__MIDI_H__)
#define __MIDI_H__

#include "mmsystem.h"
#pragma comment(lib,"winmm.lib")
//
class CMidi
{
public:
    DWORD Play(HWND,char* FileName);
    void Replay(WPARAM w);
    void Stop();
	void PlayWav(char* FileName);
//
private:
    UINT wDeviceID;//MCI装置代号
    DWORD dwReturn;
    MCI_OPEN_PARMS mciOpenParms;
    MCI_PLAY_PARMS mciPlayParms;
    MCI_STATUS_PARMS mciStatusParms;
    MCI_SEQ_SET_PARMS mciSeqSetParms;
};

#endif