//FS.h

#if !defined(__FS_H__)
#define __FS_H__

#include "Bmp.h"


//
class CFS : public CScene	//从“场”类继承
{
public:
	CFS();
	~CFS();
	BOOL Init(HWND hwnd,CMidi* pMidi,CGameData* pDB);
	void OnLButtonDown(int tx,int ty);
	void OnTimerPrc(HWND hwnd);
	virtual BOOL Open(HWND hwnd,char* FileName);		//从配置文件打开
	//用于检验指令的执行状态，只有不忙的时候才能执行下一条指令
	//图片的显示/消失，等待用户的鼠标响应，都属于忙状态
//继承的命令有：
//	BOOL  Open(char* FileName);		//从配置文件打开
//	BOOL  Save(char* FileName);		//保存入指定文件
//	ACT*  GetNextAct();				//获得下一条动作指令。
//	BOOL  CluAll(CGameData* pDB);	//执行出口过程的全部计算指令。
//

private:
	CMidi*  m_pMidi;				//声音播放类的指针
	CGameData*	m_pDB;				//系统数据堆
    CBmp	m_BkBmp;				//背景图片
	CEnemy	m_Enemy[40];			//敌人数组
	CBar	m_Bar;					//前景信息显示
	HDC		m_hBufDC;				//主缓冲层面
	HDC		m_hEnemyDC;			//1号敌人图片（敌人的所有图片是共用的）
	BOOL OnCode(HWND hwnd,ACT* pAct);	//执行一条动作指令
};

#endif