01、数据拷贝（CPY）
02、立即数赋值（SET）
03、加法指令（ADD）
04、减法指令（SUB）
05、乘法指令（MUL）
06、除法指令（DIV）
07、求余指令（MOD）
08、如果大于则赋值（IFB）
09、如果小于则赋值（IFS）
10、如果等于则赋值（IFE）
11、与运算指令（AND）
12、或运算指令（ORL）
13、非运算指令（NOT）

1、设置背景指令（SETBK）
2、播放MIDI指令（PLAYMIDI）
3、停止MIDI指令（STOPMIDI）
4、播放WAV指令（PLAYWAV）
5、图片显示指令（APPEAR）
6、图片消失指令（DISAPP）
7、对话指令（DLALOG）
8、延时指令（DELAY）
5、敌人配置指令（ENEMY）