// Scene.h: interface for the CScene class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCENE_H__2E2C904B_E495_4D31_B44D_23BFE9F0667D__INCLUDED_)
#define AFX_SCENE_H__2E2C904B_E495_4D31_B44D_23BFE9F0667D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//
//
//
//
typedef struct
{
	int nActType;	//指令类型
	int nPara1;		//第一个参数
	int nPara2;		//第二个参数
	int nPara3;		//第三个参数
	int nPara4;		//第四个参数
	int nPara5;		//第五个参数
	int nPara6;		//第六个参数
}ACT;
//
typedef struct
{
	int nCluType;	//指令类型
	int nPara1;		//第一个参数
	int nPara2;		//第二个参数
	int nPara3;		//第三个参数
	int nPara4;		//第四个参数
}CLU;
//
//
//
//
class CScene
{
//
public:
	CScene();
	virtual ~CScene();
	BOOL  New();
	BOOL  Open(char* FileName);
	BOOL  Save(char* FileName);
//
	int SetID(int nID);
	int SetType(int nType);
	int GetID();
	int GetType();
	int GetActNum();
	int GetCluNum();
//
	//添加一个动作指令
	BOOL AddAct(ACT* pAct);
	//插入一个动作指令（在nIndex的前面插入一个新的事件）
	BOOL InsertAct(int nIndex,ACT* pAct);//说明：如果往后面插入，那么第一个事件的前面就无法插入了
	//修改一个动作指令
	BOOL SetAct(int nIndex,ACT* pAct);
	//删除一个动作指令
	BOOL DelAct(int nIndex);//nIndex:0-based
	//读取一个动作指令
	BOOL GetAct(int nID,ACT* pAct);
//
	//添加一个计算指令
	BOOL AddClu(CLU* pClu);
	//插入一个计算指令（在nIndex的前面插入一个新的事件）
	BOOL InsertClu(int nIndex,CLU* pClu);//说明：如果往后面插入，那么第一个事件的前面就无法插入了
	//修改一个计算指令
	BOOL SetClu(int nIndex,CLU* pClu);
	//删除一个计算指令
	BOOL DelClu(int nIndex);//nIndex:0-based
	//读取一个计算指令
	BOOL GetClu(int nID,CLU* pClu);
//
private:
	int   m_nID;		//游戏进程ID
	int   m_nType;		//场类型
	int   m_nActNum;	//过程阶段动作指令的数目
	int   m_nCurAct;	//0
	ACT*  m_pAct;		//动作指令数组
	int   m_nCluNum;	//出口阶段计算指令的数目
	int   m_nCurClu;	//0
	CLU*  m_pClu;		//计算指令数组
//
};

#endif // !defined(AFX_SCENE_H__2E2C904B_E495_4D31_B44D_23BFE9F0667D__INCLUDED_)
