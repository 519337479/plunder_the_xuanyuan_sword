// CluList.cpp : implementation file
//
#include "stdafx.h"
#include "Editor.h"
#include "CluList.h"

#include "CluDlg.h"
#include "scene.h"
#include "EditorDoc.h"
#include "EditorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//column上面显示的文字
#define NUM_COLUMNS_B	5	//5个柱

static _TCHAR *_gszColumnLabelB[NUM_COLUMNS_B] =
{
	_T("计算指令"),
	_T("参数1"),
	_T("参数2"),
	_T("参数3"),
	_T("参数4")
};
//column上文字的显示方式（靠左）
static int _gnColumnFmtB[NUM_COLUMNS_B] = 
{
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT
};
//column的宽度
static int _gnColumnWidthB[NUM_COLUMNS_B] = 
{
	96,
	50,
	50,
	50,
	50
};
//指令名称
static _TCHAR *_CluTypeLabel[13] =
{
	_T("数据拷贝      "),
	_T("立即数赋值    "),
	_T("加法指令      "),
	_T("减法指令      "),
	_T("乘法指令      "),
	_T("除法指令      "),
	_T("求余指令      "),
	_T("如果大于则赋值"),
	_T("如果小于则赋值"),
	_T("如果等于则赋值"),
	_T("与运算        "),
	_T("或运算        "),
	_T("非运算        ")
};
/////////////////////////////////////////////////////////////////////////////
// CCluList dialog
CCluList::CCluList(CEditorView* pView,CEditorDoc* pDoc,CScene* pData)
{	//非模式对话框
	m_pView = pView;
	m_pDoc  = pDoc;
	m_pData = pData;
	//{{AFX_DATA_INIT(CCluList)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


BOOL CCluList::Create()
{
	return CDialog::Create(IDD_CLULIST);
}


void CCluList::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCluList)
	DDX_Control(pDX, IDC_LIST, m_lstCluList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCluList, CDialog)
	//{{AFX_MSG_MAP(CCluList)
	ON_COMMAND(ID_EDITONE, OnEditone)
	ON_COMMAND(ID_INSERT, OnInsert)
	ON_COMMAND(ID_DELONE, OnDelone)
	ON_COMMAND(ID_ADDONE, OnAddone)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST, OnDblclk)
	ON_NOTIFY(NM_RCLICK, IDC_LIST, OnRclick)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCluList message handlers

void CCluList::OnOK() 
{	//屏蔽Alt+F4或者回车操作
	return;
}

void CCluList::OnCancel() 
{	//屏蔽Alt+F4或者回车操作
	return;
}

BOOL CCluList::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here
	//bFirst用于保持该程序仅被执行一次
	static BOOL bFirst = TRUE;
	if(bFirst)
	{
		//设定一个用于存取column的结构lvc
		LVCOLUMN lvc;
		//设定存取模式
		lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
		//用InSertColumn函数向窗口中插入柱
		for( int i=0 ; i<NUM_COLUMNS_B ; i++ )
		{
			lvc.iSubItem = i;
			lvc.pszText = _gszColumnLabelB[i];
			lvc.cx = _gnColumnWidthB[i];
			lvc.fmt = _gnColumnFmtB[i];
			m_lstCluList.InsertColumn(i,&lvc);
		}
		//设定列表一行全部选中的特性
		m_lstCluList.SetExtendedStyle(LVS_EX_FULLROWSELECT);
		bFirst = FALSE;
	}
	UpdateList();
	//
	//
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCluList::UpdateList()
{
	char cTemp [255];//定义临时数组（用于将数字变换为字符串）
	char cPszText [255];//给pszText使用
	CLU temp;

	m_lstCluList.DeleteAllItems();//清除列表

	int nCount = m_pData->GetCluNum();//取得条目的总数
	for ( int index=0; index<nCount; index++ )
	{
		LVITEM lvi;//定义用于写入列表结构的lvi
		lvi.mask = LVIF_TEXT;//定义写入模式
		lvi.cchTextMax = 100;
		lvi.pszText = cPszText;

		m_pData->GetClu( index, &temp );

		//写入第一个柱（事件编号，文字）
		lvi.iItem = (index);//总是写入最后一行中
		strcpy(lvi.pszText,_CluTypeLabel[temp.nCluType-1]);
		lvi.iSubItem = 0;
		m_lstCluList.InsertItem(&lvi);

		//写入第二个柱（参数1）
		itoa( (temp.nPara1), cTemp, 10 );
		strcpy(lvi.pszText,cTemp);
		lvi.iSubItem = 1;
		m_lstCluList.SetItem(&lvi);

		//写入第三个柱（参数2）
		itoa( (temp.nPara2), cTemp, 10 );
		strcpy(lvi.pszText,cTemp);
		lvi.iSubItem = 2;
		m_lstCluList.SetItem(&lvi);

		//写入第四个柱（参数3）
		itoa( (temp.nPara3), cTemp, 10 );
		strcpy(lvi.pszText,cTemp);
		lvi.iSubItem = 3;
		m_lstCluList.SetItem(&lvi);

		//写入第五个柱（参数4）
		itoa( (temp.nPara4), cTemp, 10 );
		strcpy(lvi.pszText,cTemp);
		lvi.iSubItem = 4;
		m_lstCluList.SetItem(&lvi);
	}
}

void CCluList::OnEditone() 
{
	// TODO: Add your command handler code here
	CCluDlg dlg;
	CLU temp;//定义临时数组
	m_pData->GetClu(m_nIndex,&temp);//读出索引位置的内容
	//将道具名称和道具说明写入对话框相应的列表中去
	if(dlg.DoModal(&temp)==IDOK)//点击了OK，则开始向m_pList中写入数据
	{
		m_pDoc->SetModifiedFlag(TRUE);//设置修改标志
		m_pData->SetClu(m_nIndex,&temp);
	}
	//刷新显示
	UpdateList();	
}

void CCluList::OnInsert() 
{
	// TODO: Add your command handler code here
	CCluDlg dlg;
	CLU temp;//定义临时数组
	temp.nCluType = 1;
	temp.nPara1   = 0;
	temp.nPara2   = 0;
	temp.nPara3   = 0;
	temp.nPara4   = 0;
	//将道具名称和道具说明写入对话框相应的列表中去
	if(dlg.DoModal(&temp)==IDOK)//点击了OK，则开始向m_pList中写入数据
	{
		m_pDoc->SetModifiedFlag(TRUE);//设置修改标志
		m_pData->InsertClu(m_nIndex,&temp);
	}
	//刷新显示
	UpdateList();	
}

void CCluList::OnDelone() 
{
	// TODO: Add your command handler code here
	if ( IDYES == ( MessageBox("您确定要删除选中的指令吗？", NULL, MB_YESNO) ) )
	{
		m_pDoc->SetModifiedFlag(TRUE);//设置修改标志
		m_pData->DelClu(m_nIndex);//实际的删除操作
		UpdateList();
	}	
}

void CCluList::OnAddone() 
{
	// TODO: Add your command handler code here
	CCluDlg dlg;
	CLU temp;//定义临时数组
	temp.nCluType = 1;
	temp.nPara1   = 0;
	temp.nPara2   = 0;
	temp.nPara3   = 0;
	temp.nPara4   = 0;
	//将道具名称和道具说明写入对话框相应的列表中去
	if(dlg.DoModal(&temp)==IDOK)//点击了OK，则开始向m_pList中写入数据
	{
		m_pDoc->SetModifiedFlag(TRUE);//设置修改标志
		m_pData->AddClu(&temp);
	}
	//刷新显示
	UpdateList();	
}

/////////////////////////////////////////////////////////////////////////////
//鼠标双击条目
void CCluList::OnDblclk(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	//取得鼠标点击的位置
	CPoint pointTemp;
	GetCursorPos( &pointTemp );
	m_lstCluList.ScreenToClient( &pointTemp );
	UINT uFlags = 0;
	m_nIndex = m_lstCluList.HitTest( pointTemp , &uFlags );//得到被选中的行
	if ( (m_nIndex != -1) && (uFlags & LVHT_ONITEM) )
	{//在字符串上双击
		OnEditone();
	}
	else
	{//在空白处双击，则添加新的字符串
		OnAddone();
	}
	*pResult = 0;
}
/////////////////////////////////////////////////////////////////////////////
//鼠标右键点击条目
void CCluList::OnRclick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	//取得鼠标点击的位置
	CPoint point;//用于确定弹出菜单的位置
	GetCursorPos( &point );
	CPoint pointTemp = point;//用于点击判断
	m_lstCluList.ScreenToClient( &pointTemp );
	UINT uFlags = 0;
	m_nIndex = m_lstCluList.HitTest( pointTemp , &uFlags );//得到被选中的行
	if ( (m_nIndex != -1) && (uFlags & LVHT_ONITEM) )
	{
		CMenu menuPopups;
		if ( menuPopups.LoadMenu( IDR_POPUP ) )
		{
			CMenu* pMenu = menuPopups.GetSubMenu( 0 );
			pMenu->EnableMenuItem(ID_ADDONE,MF_DISABLED|MF_BYCOMMAND|MF_GRAYED);
			pMenu->TrackPopupMenu( TPM_LEFTALIGN|TPM_RIGHTBUTTON, point.x, point.y, this );
		}
	}
	else
	{
		CMenu menuPopups;
		if ( menuPopups.LoadMenu( IDR_POPUP ) )
		{
			CMenu* pMenu = menuPopups.GetSubMenu( 0 );
			pMenu->EnableMenuItem(ID_EDITONE,MF_DISABLED|MF_BYCOMMAND|MF_GRAYED);
			pMenu->EnableMenuItem(ID_DELONE,MF_DISABLED|MF_BYCOMMAND|MF_GRAYED);
			pMenu->EnableMenuItem(ID_INSERT,MF_DISABLED|MF_BYCOMMAND|MF_GRAYED);
			pMenu->TrackPopupMenu( TPM_LEFTALIGN|TPM_RIGHTBUTTON, point.x, point.y, this );
		}
	}
	*pResult = 0;
}
/////////////////////////////////////////////////////////////////////////////