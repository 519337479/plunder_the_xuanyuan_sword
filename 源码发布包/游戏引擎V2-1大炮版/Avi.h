// Avi.h: interface for the CAvi class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(__AVI_H__)
#define __AVI_H__

class CAvi
{
private:
	// 图片的显示位置
	int m_x;
	int m_y;
	// 图片的宽度
	int m_w;
	// 图片的高度
	int m_h;
	// 是否显示
	BOOL m_bShow;
	// 用于存放位图的设备
	CDC m_MemDC;

// 以下用于动画
	// 上次改变动画的时间
	long m_lLastChangeTime;
	// 单元图片在整张图片中的定位
	int m_nTileX;
	int m_nTileY;
	// 单元图片的宽度和高度
	int m_nTileW;
	int m_nTileH;
	// 单元图片排列方式：1:单元图片横向排列；2:单元图片纵向排列
	int m_nArrayMode;
	// 一个动画循环的总祯数
	int m_nTileCount;
	// 当前动画的祯(0_based)
	int m_nCurTile;
	// 动画循环的次数（-1表示无限循环）
	int m_nTimes;
	// 每祯动画间隔的时间
	int m_nInterval;
	// 动画显示完成后的处理：1:保持最后一祯；2:消失
	int m_nOverMode;
	// 每一祯动画显示位置改变的偏移量
	int m_nOffsetX;
	int m_nOffsetY;
	// 保存文件名
	char m_psFileName[256];

public:
	CAvi();
	virtual ~CAvi();

public:
	// 初始化，打开资源文件，构造必要的DC
	BOOL Init( CDC* pDC );

	// 打开文件，设置诸项参数
	// 单元图片要么横向排列，要么纵向排列。
	// 横向排列时，nArrayMode参数设置为1，nSpace是单元图片的宽度
	// 纵向排列时，nArrayMode参数设置为2，nSpace是单元图片的高度
	BOOL Open( char* psFileName,	// 文件名
			   int x,               // 初始显示位置X
			   int y,               // 初始显示位置Y
			   int nTileX,          // 动画起始单元的X
			   int nTileY,          // 动画起始单元的Y
			   int nTileW,          // 单元图片的宽度
			   int nTileH,          // 单元图片的高度
			   int nArrayMode,      // 排列方式：1:单元图片横向排列；2:单元图片纵向排列
			   int nTileCount,      // 动画循环总祯数
			   int nCurTile,        // 当前的动画祯
			   int nTimes,          // 动画循环的次数
			   int nInterval,       // 动画祯之间的间隔时间
			   int nOverMode,       // 动画显示完成后的处理：1:保持最后一祯；2:消失
			   int nOffsetX,        // 动画显示位置改变的偏移量X
			   int nOffsetY         // 动画显示位置改变的偏移量Y
			  );

	// 绘制
	void Draw( CDC* pDC, int x, int y );

	// 处理移动
	BOOL Proc( long lNow );

	// 设置是否显示
	BOOL Show( BOOL bShow );

	// 将本类保存至字符串
	BOOL SaveToString( CString& strFile );

	// 从字符串还原本类
	BOOL LoadFromString( CString& strFile );
};

#endif // !defined(__AVI_H__)
