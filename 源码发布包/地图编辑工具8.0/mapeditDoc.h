// mapeditDoc.h : interface of the CMapeditDoc class
//
/////////////////////////////////////////////////////////////////////////////
#if !defined(AFX_MAPEDITDOC_H__886788C8_D5BF_401D_A2D1_9C2DB3EBCF8D__INCLUDED_)
#define AFX_MAPEDITDOC_H__886788C8_D5BF_401D_A2D1_9C2DB3EBCF8D__INCLUDED_
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#define CAPACITY 10000//设定可以UNDO的次数

class CMapeditDoc : public CDocument
{
//███████████████████████████████████████
public:
	BOOL undo_st;//是否可以undo
	int m_bkup_counter;//定义计数器（undo的次数）
	int m_bkup_top;//定义栈顶指针（undo栈）
	int backup[CAPACITY][3];//用于存放对地图矩阵写入操作的历史数据，undo操作用
	//数据结构：队列，所以，m_bkup_counter的上限就是CAPACITY常数
public:
	void ResetBackup();//用于清空历史数据，并使undo_st指示失效。
	void save(int value,int m_x_value,int m_y_value);//向地图矩阵写入一个数据
	BOOL Load(int *pvalue,int *pm_x_value,int *pm_y_value);//从地图矩阵读出一个数据
	void Undo(void);//执行undo操作的函数

protected: // create from serialization only
	CMapeditDoc();
	DECLARE_DYNCREATE(CMapeditDoc)

// Attributes
public:
	char map[MAP_GRID_W][MAP_GRID_H][2];//定义地图矩阵
	char evt[MAP_GRID_W][MAP_GRID_H][2];//定义事件矩阵
// Operations
public:
	int ReadMap(int x, int y);//读地图，从地图矩阵x,y的位置读出一个数值
	void WriteMap(int x, int y, int z);//写地图，向地图矩阵x,y的位置写入z
	int ReadEvt(int x, int y);//读事件，从事件矩阵x,y的位置读出一个数值
	void WriteEvt(int x, int y, int z);//写事件，向事件矩阵x,y的位置写入z

	CString m_sMapFName;//地图文件名(不带路径，仅显示标题用)
	CString m_sEventFileName;//事件文件名(带路径)
	BOOL m_bEventModified;//事件是否被修改过的标志
	void SaveEventFile(void);
	void LoadEventFile(void);

//███████████████████████████████████████
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMapeditDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMapeditDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMapeditDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CMapeditDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
public:
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAPEDITDOC_H__886788C8_D5BF_401D_A2D1_9C2DB3EBCF8D__INCLUDED_)
