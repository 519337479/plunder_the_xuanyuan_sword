// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__34562091_B485_4B58_96F7_C5BF2D99742A__INCLUDED_)
#define AFX_MAINFRM_H__34562091_B485_4B58_96F7_C5BF2D99742A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define ID_ACCIDENT      (WM_USER + 100)
#define GAME_TIMER_ID    (1001)
#define ST_DIALOG_NORMAL_EXIT (1000000)

enum {
	AZIMUTH_UP = 1,
	AZIMUTH_DOWN = 2,
	AZIMUTH_LEFT = 3,
	AZIMUTH_RIGHT =4
};

class CMidi;
class CEnemy;
class CMainFrame : public CFrameWnd
{
private:
	// 用于背景音乐和声音播放的库
	CMidi* m_pMidi;

public:
//全局变量定义
//+███████████████████████████████████████
/////////////////////////////////////////////////////////////////////////////
//以下部分定义存盘时需要使用的变量。
/////////////////////////////////////////////////////////////////////////////
	int blood;//生命
	int level;//等级
	int bullet;//子弹数量
	BOOL super_bullet;//超级子弹
	int st;//系统状态号
	CPoint current;//主角当前的位置
	int azimuth;//主角的方位。1上2下3左4右
	//9上打10下打11左打12右打13上功14下功15左功16右功
	//17上枪18下枪19左枪20右枪21左上枪22右上枪23左下枪24右下枪
	int st_sub1;
	int st_sub2;
	int m_info_prop1;//第一个道具图标（信、药、货物、食品和水），0表示没有
	int m_info_prop2;//第二个道具图标（短刀或瑞士军刀），0表示没有
	int m_info_prop3;//第三个道具图标（剑），0表示没有
	int m_info_prop4;//第四个道具图标（枪），0表示没有
	int st_dialog;//对话进度号
	int st_progress;//细节进度号
	int base_x;
	int base_y;//设定地图基准点（相对于左上角）
	int score;//升级所需要的一个数值
/////////////////////////////////////////////////////////////////////////////
//以下部分定义全局变量。
/////////////////////////////////////////////////////////////////////////////
	int m_mouse_azimuth;//方向，同azimuth
	CPoint m_mouse_pointer;//当前鼠标箭头的位置
	BLENDFUNCTION bf;//用于ALPHA混合
	CBitmap bmp_start;//设计局声明
	CBitmap bmp_title;//版权声明
	CBitmap bmp_end;//结束声明
	CBitmap bmp_Bomb;//爆炸动画
	CBitmap bmp_MapLayer;//定义地图层（空图片）
	CBitmap bmp_BufLayer;//定义缓冲层（空图片）
	CBitmap bmp_Dialog;//定义对话缓冲（空图片）
	HBITMAP hbmp_Minimap;//定义小地图缓冲
	HBITMAP hbmp_Mapcils;//地图文件图片
	HBITMAP hbmp_Actor;//主角图片
	HBITMAP hbmp_temp;//用于堆放垃圾指针
	CDC MapDC;//地图层
	CDC BuffDC;//缓冲层
	CDC CilDC;//地图单元，这样做为了可以直接显示
#ifdef DEBUG
	CDC EvtDC;//显示事件编号，调试用
#endif //DEBUG
	CDC DialogDC;//用于显示对话
	CDC ActorDC;//用于存放主角
	int disdialogst;//用于指示对话系统的显示状态，1显示；0隐藏
	CDC MiniMapDC;//用于显示小地图，显示状态由m_info_map指示
	CDC BombDC;//存放爆炸动画
	CDC MemDC;//临时交换区
	int ShootingStatus;//用于射击图片选择
			//ShootingStatus
			// 1：左上
			// 2: 上
			// 3: 右上
			// 4: 左
			// 5: 默认左上
			// 6: 右
			// 7: 左下
			// 8: 下
			// 9: 右下
	int messagedelay;//防止连续触发两个消息
	int movest;//画面偏移状态：0正位1,2,3,4向上5,6,7,8向下9,10,11,12向左13,14,15,16向右
	//17,18向上功19，20向上打21,22向下功23，24向下打25,26向左功27，28向左打29,30向右功31，32向右打
	//33上枪34下枪35左枪36右枪37左上枪38右上枪39左下枪40右下枪
	int arcst;//攻击形式。和level一起决定攻击的力度。
	CPoint oldcurrent;//存留老的位置
	int map[200][200];//定义地图矩阵
	int npc[200][200];//定义事件矩阵
	HFONT Font;
	char dialogtext[320][130];//一个dlg文件中存储的对话内容
	CPen greenPen;//绿色画笔
	CPen redPen;//红色画笔
	int timelimit;//定义时间限制（仅用于最后一段）
	int timedelay;//用于时间减少速度的调整
	int a[40];//用于移动多个人物时存放前一个人物的临时状态
	char fname01[16];
	char fname02[16];
	char fname03[16];
	char oldfname01[16];
	char oldfname02[16];
	char oldfname03[16];//用于恢复地图
/////////////////////////////////////////////////////////////////////////////
//定义50个敌人。
/////////////////////////////////////////////////////////////////////////////
	CEnemy* m_enemies[50];
/////////////////////////////////////////////////////////////////////////////
//以下部分定义鼠标光标。
/////////////////////////////////////////////////////////////////////////////
	HCURSOR cur0;
	HCURSOR cur1;
	HCURSOR cur2;
	HCURSOR cur3;
	HCURSOR cur4;
	HCURSOR cur5;
	HCURSOR cur6;
	HCURSOR m_HForbiden;
/////////////////////////////////////////////////////////////////////////////
//以下部分定义临时使用的图片资源。在一个片断内所需的头像。
/////////////////////////////////////////////////////////////////////////////
	HBITMAP hbmp_Photo0;//零号人物头像128*128像素，第一区是蒙版，最多5种表情。
	HBITMAP hbmp_Photo1;//一号人物头像128*128像素，第一区是蒙版，最多5种表情。
	HBITMAP hbmp_Photo2;//二号人物头像128*128像素，第一区是蒙版，最多5种表情。
	HBITMAP hbmp_Photo3;//三号人物头像128*128像素，第一区是蒙版，最多5种表情。
	HBITMAP hbmp_Photo4;//四号人物头像128*128像素，第一区是蒙版，最多5种表情。
	HBITMAP hbmp_Photo5;//五号人物头像128*128像素，第一区是蒙版，最多5种表情。
	HBITMAP hbmp_Photo6;//六号人物头像128*128像素，第一区是蒙版，最多5种表情。
	HBITMAP hbmp_Photo7;//七号人物头像128*128像素，第一区是蒙版，最多5种表情。
	HBITMAP hbmp_Photo8;//八号人物头像128*128像素，第一区是蒙版，最多5种表情。
	HBITMAP hbmp_Photo9;//九号人物头像128*128像素，第一区是蒙版，最多5种表情。
/////////////////////////////////////////////////////////////////////////////
//以下部分定义临时使用的图片资源。在一个片断内所需的物品。
/////////////////////////////////////////////////////////////////////////////
	HBITMAP hbmp_Prop0;//零号道具，360*128像素。
	HBITMAP hbmp_Prop1;//一号道具，360*128像素。
	HBITMAP hbmp_Prop2;//二号道具，360*128像素。
	HBITMAP hbmp_Prop3;//三号道具，360*128像素。
	HBITMAP hbmp_Prop4;//四号道具，360*128像素。
	HBITMAP hbmp_Prop5;//五号道具，360*128像素。
	HBITMAP hbmp_Prop6;//六号道具，360*128像素。
	HBITMAP hbmp_Prop7;//七号道具，360*128像素。
	HBITMAP hbmp_Prop8;//八号道具，360*128像素。
	HBITMAP hbmp_Prop9;//九号道具，360*128像素。
/////////////////////////////////////////////////////////////////////////////
//以下部分定义临时使用的图片资源。在一个片断内所需的其他图片。
/////////////////////////////////////////////////////////////////////////////
	HBITMAP hbmp_0;
	HBITMAP hbmp_1;
	HBITMAP hbmp_2;
	HBITMAP hbmp_3;
	HBITMAP hbmp_4;
	HBITMAP hbmp_5;
	HBITMAP hbmp_6;
	HBITMAP hbmp_7;
	HBITMAP hbmp_8;
	HBITMAP hbmp_9;
	HBITMAP hbmp_10;
	HBITMAP hbmp_11;
	HBITMAP hbmp_12;
	HBITMAP hbmp_13;
	HBITMAP hbmp_14;
	HBITMAP hbmp_15;
	HBITMAP hbmp_16;
	HBITMAP hbmp_17;
	HBITMAP hbmp_18;
	HBITMAP hbmp_19;
	HBITMAP hbmp_20;
	HBITMAP hbmp_21;
	HBITMAP hbmp_22;
	HBITMAP hbmp_23;
	HBITMAP hbmp_24;
	HBITMAP hbmp_25;
	HBITMAP hbmp_26;
	HBITMAP hbmp_27;
	HBITMAP hbmp_28;
	HBITMAP hbmp_29;
/////////////////////////////////////////////////////////////////////////////
//以下部分定义状态条使用的变量。
/////////////////////////////////////////////////////////////////////////////
	CBitmap bmp_Info;//信息条图片
	HBITMAP hbmp_PropIcon;//显示在信息条的道具图标，32*32像素每个。
    CDC InfoDC;//信息条图片，这样做为了可以直接显示
    CDC PropIconDC;//道具图标，这样做为了可以直接显示
	int m_info_exit;//退出按钮的状态：0禁止；1使能；2预备退出态；3以上无定义
	int m_info_map;//地图按钮的状态：0禁止；1使能；2地图显示中；3以上无定义
	int m_info_next;//NEXT按钮的状态：0禁止；1使能；2已经按过；3以上无定义
	int m_info_snapshot;//截屏按钮的状态：0禁止；1使能
	int m_info_st;	//操作状态的控制：
					//100：射击态；10：战斗态；9：对话态；8：自由态；7：系统态
	int m_oldinfo_st;//用于保存m_info_st的数值
	int m_oldinfo_exit;
	int m_oldinfo_map;
	int m_oldinfo_next;
	int m_olddialog[4];
//-███████████████████████████████████████
//函数定义
//+███████████████████████████████████████
/////////////////////////////////////////////////////////////////////////////
//绘制界面所用的函数和系统函数
/////////////////////////////////////////////////////////////////////////////
	void renewscreen(CDC *pdc);
	void RenewInfo(CDC *pdc);
	void RenewInfoButtons(CDC *pdc);
	void SaveGame(void);
	BOOL LoadGame(void);
/////////////////////////////////////////////////////////////////////////////
//处理移动所用的函数
/////////////////////////////////////////////////////////////////////////////
	void MoveActor(void);//绘制移动的主角(4拍)
	void ActActor(int type);//绘制动作的主角(2拍或4拍)
	void DrawActor(int type);//绘制静止的主角(一次)type:0:写屏
	void DrawMap(void);//绘制背景地图
	void displayFightingHero(int sst,int snum,CDC* pDC);//绘制多方射击的函数
/////////////////////////////////////////////////////////////////////////////
//绘制对话所用的函数
/////////////////////////////////////////////////////////////////////////////
	void Dialog(int mode,int lface,int rface,int prop,int text);
/////////////////////////////////////////////////////////////////////////////
//处理“上下左右杀”所需要的函数
/////////////////////////////////////////////////////////////////////////////
	BOOL GoUp();
	BOOL GoDown();
	BOOL GoLeft();
	BOOL GoRight();
	void Fight();
/////////////////////////////////////////////////////////////////////////////
//五大事件处理所用的函数（最后一个是定时器消息，此处未包括）
/////////////////////////////////////////////////////////////////////////////
	void InitGame(void);
	void OnShoot(int power,CPoint point);
	void OnInfo(int type);
//	void OnAccident(int type);
/////////////////////////////////////////////////////////////////////////////
//其他函数
/////////////////////////////////////////////////////////////////////////////
	void GetPower(int type,int *power);//计算伤害，1表示用枪；2表示一般拳脚；3表示重击
	void displayenemies();//用在定时器函数中
	BOOL CreateSnapshotFolder(CString sDir);//如果目录不存在，则创建
/////////////////////////////////////////////////////////////////////////////
//引入外部文件
/////////////////////////////////////////////////////////////////////////////
#include "para1.h"
#include "para2.h"
#include "para3.h"
#include "para4.h"
#include "para5.h"
#include "para6.h"
#include "para7.h"
#include "para8.h"
//+████████████████████████████████████████
//公共的函数和变量
void PlayWave(char* filename);
void PlayMidi(char* filename);
void StopMidiPlay(void);
/////////////////////////////////////////////////////////////////////////////
//绘制单个的图片（如果动作是连续的，程序会自动覆盖老位置的图像）
int oldlx;
int oldly;
int oldx;
int oldy;

/////////////////////////////////////////////////////////////////////////////
//覆盖老位置的图像
void resetblt(void);

//l:左点坐标//xy:长宽//ms:源蒙版位置//s:源位置
void blt(int lx,int ly,int x,int y,int msx,int msy,int sx,int sy,CDC *pdc);

//动作处理
void ActProc(void);
void SetActTimer(void);
void StopActTimer(void);
void GameSleep(int ms);

//打开地图图片、地图矩阵和NPC矩阵
//fname1:地图图片；fname2:地图矩阵；fname3:NPC矩阵。该函数同时存留oldcurrent。
void OpenMap(char *fname1,char *fname2,char *fname3);

//调入主角
void LoadActor(char *fname);

//调入对话
void LoadDialog(char *fname);

//场景切换，上下和左右两种方向的展开和关闭
void lopen(void);
void lclose(void);
void vopen(void);
void vclose(void);
void quake(void);

//游戏结束的处理
void gameover(void);

//游戏的升级的处理
void levelup(void);

//敌人的处理
void processenemies(void);

//释放所有图片占用的内存
void deleteallbmpobjects(void);
//-█/////////////////////////////////////////////////////////////////////////////
//-████████████████████████████████████████

public: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	//afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnKill();
	afx_msg LRESULT OnAccident(WPARAM type, LPARAM reserve);
	afx_msg void OnExit();
	afx_msg void OnMap();
	afx_msg void OnSnapshot();
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
    afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
    afx_msg void OnNcMouseMove(UINT nHitTest, CPoint point);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__34562091_B485_4B58_96F7_C5BF2D99742A__INCLUDED_)
