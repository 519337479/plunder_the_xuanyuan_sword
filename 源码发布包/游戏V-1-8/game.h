// game.h : main header file for the GAME application
//

#if !defined(AFX_GAME_H__1EC1B8B8_FF23_490D_9A3A_875D9C2327AA__INCLUDED_)
#define AFX_GAME_H__1EC1B8B8_FF23_490D_9A3A_875D9C2327AA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CGameApp:
// See game.cpp for the implementation of this class
//

class CGameApp : public CWinApp
{
private:
	long m_lLastRenewScreenTime;    // 上次重绘屏幕的时间
	long m_lInactiveTime;           // 系统进入非活动状态的开始时间
	long m_lInactiveCount;          // 系统的累计非活动时间

public:
	BOOL m_EnableActProc;//是否启动Timer处理

public:
	CGameApp();
	void WasteMessage(void);
	CString GetLocalPath(void);
	CString GetFilePath(CString& strFullPath);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGameApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CGameApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	virtual BOOL OnIdle(LONG lCount);
};

extern CGameApp theApp;
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GAME_H__1EC1B8B8_FF23_490D_9A3A_875D9C2327AA__INCLUDED_)
