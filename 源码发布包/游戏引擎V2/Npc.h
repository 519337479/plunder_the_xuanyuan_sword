// Npc.h: interface for the CNpc class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(__NPC_H__)
#define __NPC_H__

// 动作状态与坐标转换表
const int npc_act_x[4] = { 96,32,64, 0 };
const int npc_act_y[4] = {  0, 0, 0, 0 };
// NPC的攻击距离
const double pdbFireDistance[6] = { 60.0, 90.0, 120.0, 150.0, 180.0, 210.0 };
// NPC的攻击力
const int pnNpcInjure[6] = { 100, 150, 200, 250, 300, 400 };
// 击毙NPC可获得的经验值
const int pnNpcExp[6] = { 100, 160, 220, 280, 350, 500 };

class CGameObject;

class CNpc : public CGameObject
{
public:
	// 类型，0表示已经死亡
	int m_nType;

private:
	// 生命值，最大值1000
	int m_nLife;

	// 防御/攻击力等级
	int m_nLevel;

	// 定义当前需要执行的动作
	WORD m_wAct;
	// 0x00:无动作
	// 0x01:向上走
	// 0x02:向下走
	// 0x03:向左走
	// 0x04:向右走
	// 0x10:向前走
	// 0x20:攻击
	// 0x40:死亡
	// 0x80:动作执行完成标志

	// 当前动作状态的代号
	int m_nStatus;
	//  1:面上迈步1； 21:面正迈步2；
	//  2:面下迈步1； 22:面下迈步2；
	//  3:面左迈步1； 23:面左迈步2；
	//  4:面右迈步1； 24:面右迈步2；
	// 以行走动作1作为基准动作；行走动作2为基准动作x值加96；
	//  100:死亡动画开始；108:死亡动画结束，共9桢。

	// NPC所要执行的动作序列
	int m_nActCount;
	int m_nCurAct;
	char* m_psAction;
	// 6种动作：
	// 1:向左拐绕；2:向右拐绕；3:原地等待；4:靠近角色；5:远离角色；6:攻击角色。

	// 行走动作延迟周期
	int m_nGoDelay;

	// 绘制攻击角色动画所使用的桢计数器
	int m_nHitHero;

public:
	CNpc( CGameMap* pGameMap, int nType, int x, int y );
	virtual ~CNpc();

public:
	// 绘制
	virtual void Draw( HDC hDC );

	// 获得所在格子的坐标。每个精灵只能占一个格子的位置
	// 得到以格子为单位的坐标系统（用于地图绘制和碰撞检测）
	BOOL GetGride( int* pX, int* pY );

	// 得到碰撞检测矩形
	BOOL GetHitTestRect( int* x, int* y, int* w, int* h );

	// 判断是否与另外一个物体相交
	BOOL IsObjectCut( CGameObject* pObj );

	// 移动
	virtual void Move( long lNow );

	// 移动到新位置
	void MoveToNewPos( WORD wAct );

	// 得到下一个动作
	int GetNextAction();

	// 向左转
	void TurnLeft();

	// 向右转
	void TurnRight();

	// 向前走
	void GoForward();

	// 靠近角色
	void CloseUp();

	// 远离角色
	void RunAway();

	// 攻击角色
	void Fire();

	// 遭受攻击
	void BeHit( int nPower );

	// 角色运动
	void GoUp();
	void GoDown();
	void GoLeft();
	void GoRight();

	//是不是棘球？
	BOOL IsSpikyBall();
};

#endif // !defined(__NPC_H__)
