// Boss.h

#if !defined(__BOSS_H__)
#define __BOSS_H__


// 攻击力矩阵
const int g_pnBossInjure[3][3] = {  4000, 3000, 1500,
								    3000, 2500, 1000,
								    1500, 1000, 500,
								 };
// 击毙BOSS可获得的经验值
#define BOSS_EXP      60000
#define BOSS_READY    1
#define BOSS_ATTACK   2
#define BOSS_DEAD     100

class CNpc;
class CBoss : public CGameObject
{
	// BOSS死亡时发出的事件通知
	int m_nEvent;

	// 生命值，最大值10000
	int m_nLife;

	// 当前动作状态的代号
	int m_nStatus;
	//  1:准备；
	//  2:动作1；
	//  3:动作2；
	//  4:动作3；
	//  5:动作4；

	// 攻击动作计数器
	int m_nActCount;

	// 攻击移动的方向
	int m_nOffset_x;
	int m_nOffset_y;

public:
	CBoss( CGameMap* pGameMap, int x, int y, int nEvent );
	virtual ~CBoss();

public:
	// 绘制
	virtual void Draw( HDC hDC );

	// 移动
	virtual void Move( long lNow );

	// 计算攻击动作的运动偏移量
	void CalcOffset();

	// 判断自己是否可以被显示
	BOOL CanBeShown();

	// 得到碰撞检测矩形
	BOOL GetHitTestRect( int* x, int* y, int* w, int* h );

	// 获得所在格子的坐标。每个精灵只能占一个格子的位置
	// 得到以格子为单位的坐标系统（用于地图绘制和碰撞检测）
	BOOL GetGride( int* pX, int* pY );

	// 攻击角色
	void Fire();

	// 遭受攻击
	void BeHit( int nPower );
};

#endif // !defined(__BOSS_H__)